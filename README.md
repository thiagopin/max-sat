# README #

Trabalho de PAA, métodos para encontrar uma boa solução (ou solução ótima) do problema MAX-SAT.

## Execução via arquivo executável

Para executar o trabalho `a partir` do `jar` gerado, certifique-se que o `Java 8` esteja devidamente instalado na sua máquina, baixe o arquivo [max-sat.jar](https://bitbucket.org/thiagopin/max-sat/downloads/max-sat.jar) e em seguida execute o comando:

    java -jar max-sat.jar <nome-arquivo> <log> <metodo>

Onde as palavras em destaque são parâmetros obrigatórios onde:
* nome-arquivo: nome do arquivo ou caminho do arquivo contendo a instância a ser executada.
* log: indica se informações adicionais serão exibidas no terminal, pode assumir os valores `true` ou `false`.
* metodo: indica que método será utilizado na busca da solução, pode assumir os valores `ga` para Algoritmos Genético (GA) ou `cp` para Programação com restrições (CP).

Segue alguns exemplos de uso:

Para execução da instância jnh19.sat, exibindo logs e usando GA:

    java -jar max-sat.jar jnh19.sat true ga

Para execução da instância jnh201.sat, não exibindo logs e usando CP:

    java -jar max-sat.jar jnh201.sat false cp

## Execução via código fonte
Para executar o trabalho `a partir` do código fonte deve-se seguir os seguintes passos:

* Instalar o git.
* Instalar o `Java 8`.
* Clonar o repositório com o código fonte.
* Executar o comando gradle de execução da solução.

Os passos podem ser reproduzidos pelos seguintes comandos (via terminal):

    git clone https://thiagopin@bitbucket.org/thiagopin/max-sat.git
    cd max-sat
    ./gradlew run -Parq=jnh4.sat -Plog=true #(para linux e mac)
    gradlew.bat run -Parq=jnh4.sat -Plog=true #(para windows)

Onde o parâmetro `-Parq` é o arquivo desejado na execução e `-Plog=true` exibe
nos terminal a evolução das gerações, nos comandos acima o arquivo jnh4.sat foi
usado como exemplo (mas outros poderiam ser utilizados) e `-Plog=true` faz com
que o valor das gerações sejam exibidas, caso contrario `-Plog=false` deveria
ser usado.
