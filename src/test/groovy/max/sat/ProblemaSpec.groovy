package max.sat

import spock.lang.*

class ProblemaSpec extends Specification {

    def "Valor do Problema"() {
        given:
        def p = new Problema(4, 5, [10, 20, 30, 40], [[-1, 3], [2, 5, 1, -3], [1, -2, 4], [-4, 5]])

        expect:
        p.valor(solucao) == esperado

        where:
        solucao         || esperado
        [1, 0, 1, 1, 0] || 60
        [0, 1, 1, 0, 0] || 70
        [0, 0, 0, 0, 0] || 100
    }

}
