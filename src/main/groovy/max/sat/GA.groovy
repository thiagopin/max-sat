package max.sat

import groovy.transform.Immutable
import groovy.transform.Memoized
import groovy.util.logging.Slf4j
import org.uncommons.maths.binary.BitString;
import org.uncommons.maths.random.MersenneTwisterRNG;
import org.uncommons.maths.random.Probability;
import org.uncommons.watchmaker.framework.EvolutionaryOperator;
import org.uncommons.watchmaker.framework.FitnessEvaluator;
import org.uncommons.watchmaker.framework.GenerationalEvolutionEngine;
import org.uncommons.watchmaker.framework.PopulationData;
import org.uncommons.watchmaker.framework.factories.BitStringFactory;
import org.uncommons.watchmaker.framework.islands.IslandEvolutionObserver;
import org.uncommons.watchmaker.framework.operators.BitStringCrossover;
import org.uncommons.watchmaker.framework.operators.BitStringMutation;
import org.uncommons.watchmaker.framework.operators.EvolutionPipeline;
import org.uncommons.watchmaker.framework.selection.RouletteWheelSelection;
import org.uncommons.watchmaker.framework.termination.TargetFitness;
import org.uncommons.watchmaker.framework.termination.Stagnation;
import org.uncommons.watchmaker.framework.termination.GenerationCount;
import java.util.Random

/**
 * Classe responsável por usar a biblioteca Watchmaker, que é uma implementação
 * de GA.
 */
@Immutable
public class GA implements Solucionador {

  //Definição do problema a ser solucionado.
  Problema problema

  private Random rng = new MersenneTwisterRNG()

  List<Byte> solucaoInicial() {
    memSolucaoInicial()
  }

  @Memoized
  List<Byte> memSolucaoInicial() {
    (0..<problema.n).collect{ rng.nextInt(2) }
  }

  @Override
  List<Byte> solucionar(boolean logging) {
    solucionar(100, 0.9, 0.1, 3, 1, logging)
  }

  /**
   * Método que usa um GA para encontrar uma solução que maximize o valor do
   * problema.
   */
  List<Byte> solucionar(int population, double crossover, double mutation,
      int points, int eliteCount, boolean logging) {
    int length = problema.n
    List<EvolutionaryOperator<BitString>> operators = new ArrayList<EvolutionaryOperator<BitString>>(2);
    operators.add(new BitStringCrossover(points, new Probability(crossover)));
    operators.add(new BitStringMutation(new Probability(mutation)));
    EvolutionaryOperator<BitString> pipeline = new EvolutionPipeline<BitString>(operators);
    GenerationalEvolutionEngine<BitString> engine = new GenerationalEvolutionEngine<BitString>(new BitStringFactory(length),
                                                                                               pipeline,
                                                                                               new BitStringEvaluator(),
                                                                                               new RouletteWheelSelection(),
                                                                                               rng);
    engine.addEvolutionObserver(new EvolutionLogger<BitString>(logging));
    return bitStringToBytes(engine.evolve(population, // total de individuos em cada geração
                         eliteCount, // Usa elitismo de um individuo.
                         [new BitString(solucaoInicial().join(''))],
                         new Stagnation(400, true))); // Continua até estagnar em 200 gerações sem melhoras.
                        //  new GenerationCount(1000)));
  }

  /**
   * Método que converte um BitString em um List<Byte>.
   * @param candidate BitString a ser convertido.
   * @return lista binária convertida.
   */
  private List<Byte> bitStringToBytes(BitString candidate) {
    (0..<candidate.length).collect { candidate.getBit(it) ? 1 : 0 }
  }

  private class BitStringEvaluator implements FitnessEvaluator<BitString> {
    /**
     * Calcula o fitness da solução candidata.
     * @param candidate O bitstring a ser avaliado.
     * @param population População no momento da avaliação.
     * @return fitness ou valor do problema ao aplicar a solução candidata.
     */
    double getFitness(BitString candidate, List<? extends BitString> population) {
        problema.valor(bitStringToBytes(candidate))
    }

    /**
     * Se true significa que o a solução deve maximizar o valor do problema.
     * @return true.
     */
    boolean isNatural() {
      true
    }
  }

  /**
   * Implementação de um logger da execução da evolução das soluções.
   */
  private class EvolutionLogger<T> implements IslandEvolutionObserver<T> {
    private boolean logging

    public EvolutionLogger(boolean logging) {
      this.logging = logging
    }

    public void populationUpdate(PopulationData<? extends T> data) {
        if(logging) println "Geração ${data.generationNumber}: ${data.bestCandidateFitness}"
    }

    public void islandPopulationUpdate(int islandIndex, PopulationData<? extends T> populationData) {
        // Do nothing.
    }
  }
}
