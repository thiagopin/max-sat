package max.sat

public class Main {

    InputStream file(String file) {
        def is = this.class.classLoader.getResourceAsStream(file)
        is ?: new FileInputStream(file)
    }

    static void periodoExecucao(Closure cback) {
        long init = System.currentTimeMillis()
        cback()
        long tempoExecucao = System.currentTimeMillis() - init
        println "Solução encontrada em ${tempoExecucao} ms"
    }

    public static void main(String[] args) {
        if(!(args && args.length == 3)) {
            println "parametros obrigatorios: <nome-arquivo> <log> <metodo>"
            println "ex.:"
            println "java -jar max-sat.jar jnh19.sat true ga"
            println "java -jar max-sat.jar jnh201.sat false cp"
            return
        }

        def file = args ? args[0] : "jnh4.sat"
        def log = args ? args[1].toBoolean() : false
        def method = args ? args[2] : "ga"

        def p = Parser.lerConverte(new Main().file(file))
//        def p = new Problema(5, 4, [10, 20, 30, 40], [[-1, 3], [2, 5, 1, -3], [1, -2, 4], [-4, 5]])

        periodoExecucao {
            def solver = method == 'ga' ? new GA(p) : new CSP(p)
            println "Resumo de $file"
            println "Valor da solução inicial: ${p.valor(solver.solucaoInicial())}"
            def solution = solver.solucionar(log)
            println "Valor da solução final: ${p.valor(solution)}"
            println "Solução final:"
            println solution.join(' ')
        }

    }
}
