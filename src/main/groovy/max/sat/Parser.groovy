package max.sat

import groovy.transform.Immutable

/**
 * Classe responsável por ler os arquivos de entrada.
 */
public class Parser {

  @Immutable
  public static class DataRep {
    int n, m
    List<List<Integer>> data
  }

  static DataRep ler(InputStream input) {
    String content = input.text.trim()
    if(content.split('\n').first().split(/\s+/).size() == 2)
    dataRep1(content) else dataRep2(content)
  }

  static dataRep1(String content) {
    List<String> lines = content.split(/\s+/)
    def (n, m) = lines[0..1]*.toInteger()
    List<Integer> body = lines[2..-1]*.toInteger()
    int idx = 0
    List<List<Integer>> pages = []
    while(idx < body.size()) {
      pages << body[idx..(idx + body[idx]+1)]
      idx += body[idx]+2
      assert pages.last().first() == pages.last().size()-2
    }
    new DataRep(n, m, pages)
  }

  static dataRep2(String content) {
    List<String> lines = content.split('\n')
    def (n, m, t) = lines.first().trim().split(/\s+/)*.toInteger()
    new DataRep(n, m, lines[1..-1].collect { [0, 1] + it.split(/\s+/)*.toInteger() })
  }

  static Problema converte(DataRep dataRep) {
    Problema p = new Problema(dataRep.n, dataRep.m, dataRep.data.collect { it[1] }, dataRep.data.collect{ it[2..-1] })
    p.assertProblema()
    p
  }

  static Problema lerConverte(InputStream input) {
    DataRep data = Parser.ler(input)
    converte(data)
  }
}
