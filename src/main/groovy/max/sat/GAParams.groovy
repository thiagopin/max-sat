package max.sat

import groovy.util.logging.Log
import groovy.util.logging.Slf4j
import org.apache.commons.mail.DefaultAuthenticator
import org.apache.commons.mail.EmailAttachment
import org.apache.commons.mail.MultiPartEmail
import org.apache.commons.mail.SimpleEmail

/**
 * Created by thiago on 06/11/15.
 */
@Slf4j
class GAParams {

    public List executarTeste() {
        def tentativas = [1, 2, 3, 4, 5]
        def populacoes = [100, 150, 200]
        def crossovers = [0.2, 0.5, 0.9, 1]
        def mutacoes = [0.01, 0.05, 0.1]
        def pontosCross = [1, 2, 3]
        def elitismos = [0, 1]
        def instancias = ['jnh4.sat', 'jnh201.sat', 'jnh211.sat', 'jnh217.sat', 'jnh302.sat']

        log.info('Iniciando execução!')
        def main = new Main()
        [['id', 'população', 'crossover', 'mutação', 'pontos', 'elitismo', 'solução', 'tempo', 'arquivo']] +
                ([populacoes, crossovers, mutacoes, pontosCross, elitismos, instancias].combinations()
                        .indexed().collectMany { k, v ->
                    tentativas.collect {
                        def (pop, cross, mut, pontos, elite, instancia) = v
                        def p = Parser.lerConverte(main.file(instancia))
                        long init = System.currentTimeMillis()
                        def solucao = new GA(p).solucionar(pop, cross, mut, pontos, elite, false)
                        long tempoExecucao = System.currentTimeMillis() - init

                        def res = [k + 1, pop, cross, mut, pontos, elite, p.valor(solucao), tempoExecucao, instancia]
                        log.info("Resultado: ${res.join(', ')}")
                        res
                    }
                })
    }

    public void testar() {
        def resultado = executarTeste().collect { it.join(';') }.join('\n')

        new File('./resultado.csv').text = resultado

        log.info("Fim Execução")
    }

    public static void main(String[] args) {
        new GAParams().testar()
    }
}
