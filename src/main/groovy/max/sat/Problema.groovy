package max.sat

import groovy.transform.Immutable
import groovy.transform.TypeChecked

/**
 * Classe que representa o problema, suas variáveis e
 */
@Immutable
@TypeChecked
class Problema {
  int n, m
  List<Integer> pesos
  List<List<Integer>> posicoes

  /**
   * Método que aplica uma possível solução ao problema e retorna o valor do
   * problema dado a solução.
   */
  int valor(List<Byte> solucao) {
    posicoes.inject(count: 0, soma: 0) { acc, eq ->
      int soma = acc['soma'] + (eq.any {
        byte aux = solucao[it.abs()-1]
        it < 0  ? !aux : aux
      } ? pesos[acc['count']] : 0)
      [count: acc['count'] + 1, soma: soma]
    }['soma'] as int
  }

  public void assertProblema() {
    assert n == ((List<Integer>) posicoes.flatten())*.abs().unique().size()
    assert m == posicoes.size()
  }

}
