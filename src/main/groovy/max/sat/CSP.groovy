package max.sat

import groovy.transform.Immutable
import org.chocosolver.solver.ResolutionPolicy
import org.chocosolver.solver.Solver
import org.chocosolver.solver.constraints.ICF
import org.chocosolver.solver.constraints.SatFactory
import org.chocosolver.solver.search.loop.monitors.IMonitorOpenNode
import org.chocosolver.solver.search.loop.monitors.SMF
import org.chocosolver.solver.trace.Chatterbox
import org.chocosolver.solver.variables.BoolVar
import org.chocosolver.solver.variables.IntVar
import org.chocosolver.solver.variables.VF

/**
 * Created by thiago on 16/06/15.
 */
@Immutable
public class CSP implements Solucionador {
    Problema problema

    List<Byte> solucaoInicial() {
        (0..<problema.n).collect { 0 }
    }

    @Override
    List<Byte> solucionar(boolean logging) {
        Solver solver = new Solver("Max Sat solver")

        BoolVar[] x = VF.boolArray("x", problema.n, solver)
        BoolVar[] s = VF.boolArray("s", problema.m, solver)
        IntVar res = VF.integer("res", 0, problema.pesos.sum(), solver)

        (0..<problema.m).each { int idx ->
            BoolVar[] ors = problema.posicoes[idx].collect {
                def aux = x[it.abs() - 1]
                it < 0 ? aux.not() : aux
            } as BoolVar[]

            SatFactory.addBoolOrArrayEqVar(ors, s[idx])
        }
        IntVar[] aux = (0..<problema.m).collect { int it -> VF.scale(s[it], problema.pesos[it]) } as IntVar[]
        solver.post(ICF.sum(aux, res))
        //LNSFactory.rlns(solver, x, 30, 20150909L, new FailCounter(100));
        SMF.limitTime(solver, 5 * 60 * 1000)
        //solver.set(ISF.domOverWDeg(x, 0, ISF.max_value_selector()))

        if (logging) solver.plugMonitor(new ConsoleLog(solver))
        solver.findOptimalSolution(ResolutionPolicy.MAXIMIZE, res)
        if (logging) Chatterbox.printStatistics(solver)
        x.collect { it.value as byte }
    }

    private class ConsoleLog implements IMonitorOpenNode {
        private Solver solver

        private Number bestSolution = 0

        ConsoleLog(Solver solver) {
            this.solver = solver
        }

        @Override
        void beforeOpenNode() {

        }

        @Override
        void afterOpenNode() {
            if (solver.measures.solutionCount > 0 &&
                    solver.objectiveManager.bestSolutionValue > bestSolution) {
                bestSolution = solver.objectiveManager.bestSolutionValue
                println(bestSolution)
            }
        }
    }
}
