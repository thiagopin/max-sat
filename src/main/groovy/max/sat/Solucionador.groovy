package max.sat

/**
 * Created by thiago on 28/06/15.
 */
interface Solucionador {

    List<Byte> solucionar(boolean log)

    List<Byte> solucaoInicial()
}
